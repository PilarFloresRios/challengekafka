package com.soprasteria.challenge.producer.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.cloud.stream.test.binder.MessageCollector;
import org.springframework.test.context.junit4.SpringRunner;

import com.soprasteria.challenge.producer.dtos.RequestShippingDTO;
import com.soprasteria.challenge.producer.streams.MessageStream;
import com.soprasteria.challenge.producer.utils.Generators;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SendServiceTest {

	@Autowired
	private MessageCollector messageCollector;

	@Autowired
	private MessageStream messageStream;;

	@SpyBean
	private SendService sendService;

	@Test
	public void whenSendRequestOrder_thenSendMessage() {
		Map<String, List<RequestShippingDTO>> map = Generators.createMapOrdes();

		sendService.sendModifiedOrders(map);

		String receivedMsg = (String) messageCollector.forChannel(messageStream.messageOut()).poll().getPayload();

		assertThat(this.sendService).isNotNull();
		assertThat(receivedMsg).isNotNull();
	}

	@Test
	public void whenSendErrorOrder_thenSendMessage() {
		String message = "Mensaje de error";

		sendService.sendErrorOrders(message);

		String receivedMsg = (String) messageCollector.forChannel(messageStream.errorOut()).poll().getPayload();

		assertThat(this.sendService).isNotNull();
		assertThat(receivedMsg).isNotNull();
	}

}
