package com.soprasteria.challenge.producer.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import com.soprasteria.challenge.producer.controllers.impl.ProducerControllerImpl;
import com.soprasteria.challenge.producer.dtos.RequestShippingDTO;
import com.soprasteria.challenge.producer.services.ProducerService;
import com.soprasteria.challenge.producer.services.SendService;
import com.soprasteria.challenge.producer.utils.Generators;

@RunWith(MockitoJUnitRunner.class)
public class ProducerControllerTest {

	@Mock
	ProducerService producerService;
	
	@Mock
	SendService sendService;
	
	@InjectMocks
	ProducerController producerController = new ProducerControllerImpl(producerService, sendService);
	
	
	
	
	@Test
	public void whenReceivedCorrectMessage_thenProcessMessageOk() {
		
		List<RequestShippingDTO> messageIn = Generators.createRequestInComplete();
		
		Map<String, List<RequestShippingDTO>> requestsMap = Generators.createMapOrdes();
		
		when(producerService.processOrdersIn(messageIn)).thenReturn(Optional.of(requestsMap));
		
		
		assertEquals(producerController.processMessage(messageIn).getStatusCode(), HttpStatus.ACCEPTED);
	}
	
	
}
