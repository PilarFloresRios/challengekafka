package com.soprasteria.challenge.producer.services;


import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.soprasteria.challenge.producer.dtos.RequestShippingDTO;
import com.soprasteria.challenge.producer.services.impl.ProducerServiceImpl;
import com.soprasteria.challenge.producer.utils.Generators;

@RunWith(MockitoJUnitRunner.class)
public class ProducerServiceTest {

	@InjectMocks
	ProducerService producerService = new ProducerServiceImpl();
	
	
	
	@Test
	public void processOrdersInWithOneCorrectRequestOrderThenOK(){
			
		List<RequestShippingDTO> requestIn = Generators.createRequestInComplete();
		Map<String, List<RequestShippingDTO>> map = Generators.createMapOrdes();
		Optional<Map<String,List<RequestShippingDTO>>> expecteds = Optional.of(map);
		
		Optional<Map<String,List<RequestShippingDTO>>> actuals = producerService.processOrdersIn(requestIn);
		
		Assert.assertEquals(expecteds.toString(), actuals.toString());
	}
}
