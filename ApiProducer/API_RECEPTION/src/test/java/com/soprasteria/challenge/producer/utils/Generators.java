package com.soprasteria.challenge.producer.utils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.soprasteria.challenge.producer.dtos.RequestShippingDTO;

public class Generators {

	public static List<RequestShippingDTO> createRequestInComplete() {
		List<RequestShippingDTO> requestIn = new ArrayList<>();

		requestIn.add(createRequestShippingDTOComplete());

		return requestIn;
	}

	public static RequestShippingDTO createRequestShippingDTOComplete() {

		RequestShippingDTO dto = new RequestShippingDTO.Builder().withCompany("Compañia de prueba")
				.withShippingAddress("Direccion de prueba").withShippingCity("Ciudad de prueba")
				.withShippingDate(LocalDate.now()).withWeight(1f).build();

		return dto;
	}

	public static Map<String, List<RequestShippingDTO>> createMapOrdes() {
		Map<String, List<RequestShippingDTO>> map = new HashMap<String, List<RequestShippingDTO>>();

		map.put("CIUDAD DE PRUEBA", createRequestInComplete());
		return map;
	}
	
	public static List<RequestShippingDTO> createRequestInWithRequestWhitoutCompany() {
		List<RequestShippingDTO> requestIn = new ArrayList<>();

		requestIn.add(createRequestShippingDTOWithoutCompany());

		return requestIn;
	}

	public static RequestShippingDTO createRequestShippingDTOWithoutCompany() {

		RequestShippingDTO dto = new RequestShippingDTO.Builder()
				.withShippingAddress("Direccion de prueba").withShippingCity("Ciudad de prueba")
				.withShippingDate(LocalDate.now()).withWeight(1f).build();

		return dto;
	}
}
