package com.soprasteria.challenge.producer.configurations;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
public class ProducerConfiguration {

	
	   @Bean
	    public LocalValidatorFactoryBean validator(MessageSource messageSource) {
	        LocalValidatorFactoryBean validatorFactoryBean = new LocalValidatorFactoryBean();
	        validatorFactoryBean.setValidationMessageSource(messageSource);
	        return validatorFactoryBean;
	    }
}
