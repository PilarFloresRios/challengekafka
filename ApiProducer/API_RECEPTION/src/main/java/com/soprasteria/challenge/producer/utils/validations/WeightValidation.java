package com.soprasteria.challenge.producer.utils.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.soprasteria.challenge.producer.utils.annotations.Weight;

public class WeightValidation implements ConstraintValidator<Weight, Float> {

	@Override
	public void initialize(Weight weight) {
	}

	@Override
	public boolean isValid(Float value, ConstraintValidatorContext context) {

		return Float.compare(value, 0f) >= 0 ? true : false;

	}

}
