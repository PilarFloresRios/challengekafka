package com.soprasteria.challenge.producer.services;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.soprasteria.challenge.producer.dtos.RequestShippingDTO;

public interface ProducerService {

	public abstract Optional<Map<String, List<RequestShippingDTO>>> processOrdersIn(
			List<RequestShippingDTO> requestShipping);

}
