package com.soprasteria.challenge.producer.dtos;

import java.util.List;

import org.springframework.http.HttpStatus;


public class ApiErrorDTO {

	
	private HttpStatus status;
	private String message;
	private List<String> errors;
	
	public ApiErrorDTO() {

	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	

	
	@Override
	public String toString() {
		StringBuilder builder2 = new StringBuilder();
		builder2.append("ApiErrorDTO [status=");
		builder2.append(status);
		builder2.append(", message=");
		builder2.append(message);
		builder2.append(", errors=");
		builder2.append(errors);
		builder2.append("]");
		return builder2.toString();
	}




	public static class Builder {
		private HttpStatus status;
		private String message;
		private List<String> errors;

		public Builder(HttpStatus status) {
			this.status = status;
		}

		public Builder withMessage(String message) {
			this.message = message;
			return this;
		}

		public Builder withErrors(List<String> errors) {
			this.errors = errors;
			return this;
		}

		public ApiErrorDTO build() {
			ApiErrorDTO ApiErrorDTO = new ApiErrorDTO();

			ApiErrorDTO.status = this.status;
			ApiErrorDTO.message = this.message;
			ApiErrorDTO.errors = this.errors;

			return ApiErrorDTO;
		}
	}
}
