package com.soprasteria.challenge.producer.exceptions;

public class ChallengeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String errorCode;

	public ChallengeException(String message, String errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
