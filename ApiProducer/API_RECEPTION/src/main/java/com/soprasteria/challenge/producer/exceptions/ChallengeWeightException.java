package com.soprasteria.challenge.producer.exceptions;

public class ChallengeWeightException extends ChallengeException{

	private static final long serialVersionUID = 1L;

	public ChallengeWeightException(String message, String errorCode) {
		super(message, errorCode);
		
	}

}
