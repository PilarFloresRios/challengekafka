package com.soprasteria.challenge.producer.utils.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.soprasteria.challenge.producer.utils.validations.WeightValidation;


@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = WeightValidation.class)
public @interface Weight {

	
	String message() default "{order.weight}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}

