package com.soprasteria.challenge.producer.controllers.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.soprasteria.challenge.producer.controllers.ProducerController;
import com.soprasteria.challenge.producer.dtos.RequestShippingDTO;
import com.soprasteria.challenge.producer.services.ProducerService;
import com.soprasteria.challenge.producer.services.SendService;

@Validated
@RestController
@RequestMapping("producer")
public class ProducerControllerImpl implements ProducerController {

	private ProducerService producerService;
	
	private SendService sendService;

	@Autowired
	public ProducerControllerImpl(ProducerService producerService, SendService sendService) {
		this.producerService = producerService;
		this.sendService = sendService;
	}

	@Override
	@PostMapping
	public ResponseEntity<Void> processMessage(List<@Valid RequestShippingDTO> requestShipping) {

		Optional<Map<String, List<RequestShippingDTO>>> processedOrders = producerService
				.processOrdersIn(requestShipping);

		sendService.sendModifiedOrders(processedOrders.orElseThrow(IllegalStateException::new));

		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

}
