package com.soprasteria.challenge.producer.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.soprasteria.challenge.producer.dtos.RequestShippingDTO;

public interface ProducerController {

	public abstract ResponseEntity<Void> processMessage(@RequestBody List<@Valid RequestShippingDTO> requestShipping);

}
