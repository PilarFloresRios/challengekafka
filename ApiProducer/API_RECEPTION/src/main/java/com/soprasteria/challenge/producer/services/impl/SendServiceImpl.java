package com.soprasteria.challenge.producer.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

import com.soprasteria.challenge.producer.dtos.RequestShippingDTO;
import com.soprasteria.challenge.producer.services.SendService;
import com.soprasteria.challenge.producer.streams.MessageStream;

@Service
public class SendServiceImpl implements SendService {

	MessageStream messageStream;

	@Autowired
	public SendServiceImpl(MessageStream messageStream) {
		this.messageStream = messageStream;
	}

	
	@Override
	public void sendModifiedOrders(Map<String, List<RequestShippingDTO>> map) {

		
		final MessageChannel messageChannel = messageStream.messageOut();
		
		messageChannel.send(MessageBuilder.withPayload(map)
				.setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
				.build());
	}

	@Override
	public void sendErrorOrders(String exception) {
		final MessageChannel messageChannel = messageStream.errorOut();
		
		messageChannel.send(MessageBuilder.withPayload(exception)
				.setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
				.build());
		
	}

}
