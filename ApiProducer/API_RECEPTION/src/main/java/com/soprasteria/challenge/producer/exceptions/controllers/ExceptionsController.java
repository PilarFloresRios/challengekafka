package com.soprasteria.challenge.producer.exceptions.controllers;

import java.util.Arrays;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.soprasteria.challenge.producer.dtos.ApiErrorDTO;
import com.soprasteria.challenge.producer.services.SendService;

@RestControllerAdvice
public class ExceptionsController {

	SendService sendService;

	@Autowired
	public ExceptionsController(SendService sendService) {
		this.sendService = sendService;
	}

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ApiErrorDTO> handle(ConstraintViolationException ex) {

		ApiErrorDTO apiErrorDto = new ApiErrorDTO.Builder(HttpStatus.BAD_REQUEST).withMessage(ex.getMessage())
				.withErrors(ex.getConstraintViolations().stream().map((i) -> i.toString()).collect(Collectors.toList()))
				.build();

		sendService.sendErrorOrders(apiErrorDto.toString());

		return new ResponseEntity<ApiErrorDTO>(apiErrorDto, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(InvalidFormatException.class)
	public ResponseEntity<ApiErrorDTO> handle(InvalidFormatException ex) {

		ApiErrorDTO apiErrorDto = new ApiErrorDTO.Builder(HttpStatus.BAD_REQUEST)
				.withMessage("Formato invalido de fecha").withErrors(Arrays.asList(ex.getMessage())).build();

		sendService.sendErrorOrders(apiErrorDto.toString());

		return new ResponseEntity<ApiErrorDTO>(apiErrorDto, HttpStatus.BAD_REQUEST);

	}

}
