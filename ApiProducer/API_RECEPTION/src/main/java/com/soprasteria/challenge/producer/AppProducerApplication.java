package com.soprasteria.challenge.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;


import com.soprasteria.challenge.producer.streams.MessageStream;

@SpringBootApplication
@EnableBinding(MessageStream.class)
public class AppProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppProducerApplication.class, args);
	}

}
