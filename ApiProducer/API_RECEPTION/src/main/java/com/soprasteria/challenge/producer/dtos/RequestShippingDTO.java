package com.soprasteria.challenge.producer.dtos;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.soprasteria.challenge.producer.utils.annotations.Weight;

public class RequestShippingDTO {

	@NotBlank(message = "{order.company.notBlank}")
	private String company;

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonFormat(pattern = "dd/MM/yy")
	private LocalDate shippingDate;

	@NotBlank(message = "{order.city.notBlank}")
	private String shippingCity;

	@NotBlank(message = "{order.address.notBlank}")
	private String shippingAddress;

	@Weight
	private float weight;

	public RequestShippingDTO() {

	}

	public RequestShippingDTO(String company, LocalDate shippingDate, String shippingCity, String shippingAddress,
			float weight) {
		this.company = company;
		this.shippingDate = shippingDate;
		this.shippingCity = shippingCity;
		this.shippingAddress = shippingAddress;
		this.weight = weight;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public LocalDate getShippingDate() {
		return shippingDate;
	}

	public void setShippingDate(LocalDate shippingDate) {
		this.shippingDate = shippingDate;
	}

	public String getShippingCity() {
		return shippingCity;
	}

	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RequestShippingDTO [company=");
		builder.append(company);
		builder.append(", shippingDate=");
		builder.append(shippingDate);
		builder.append(", shippingCity=");
		builder.append(shippingCity);
		builder.append(", shippingAddress=");
		builder.append(shippingAddress);
		builder.append(", weight=");
		builder.append(weight);
		builder.append("]");
		return builder.toString();
	}

	public static class Builder {
		private String company;
		private LocalDate shippingDate;
		private String shippingCity;
		private String shippingAddress;
		private float weight;
		
		public Builder() {
			
		}
		
		public Builder withCompany(String company) {
			this.company = company;
			return this;
		}
		
		public Builder withShippingDate(LocalDate shippingDate) {
			this.shippingDate = shippingDate;
			return this;
			
		}
		
		public Builder withShippingCity(String shippingCity) {
			this.shippingCity = shippingCity;
			return this;
		}
		
		public Builder withShippingAddress(String shippingAddress) {
			this.shippingAddress = shippingAddress;
			return this;
		}
		
		public Builder withWeight(float weight) {
			this.weight = weight;
			return this;
		}
		
		public RequestShippingDTO build() {
			RequestShippingDTO dto = new RequestShippingDTO();
			dto.company = this.company;
			dto.shippingAddress = this.shippingAddress;
			dto.shippingCity = this.shippingCity;
			dto.shippingDate = this.shippingDate;
			dto.weight = this.weight;
			
			return dto;
		}

	}

}
