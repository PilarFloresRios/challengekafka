package com.soprasteria.challenge.producer.streams;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface MessageStream {

	String OUTPUT = "message-out";
	

	String OUTPUT_ERROR = "error-out";


	@Output(OUTPUT)
	MessageChannel messageOut();
	
	@Output(OUTPUT_ERROR)
	MessageChannel errorOut();
}
