package com.soprasteria.challenge.producer.services.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.soprasteria.challenge.producer.dtos.RequestShippingDTO;
import com.soprasteria.challenge.producer.services.ProducerService;

@Service
public class ProducerServiceImpl implements ProducerService {

	@Override
	public Optional<Map<String, List<RequestShippingDTO>>> processOrdersIn(List<RequestShippingDTO> requestShipping) {

		return Optional.of(requestShipping.stream()
				.collect(Collectors.groupingBy(o -> o.getShippingCity().toUpperCase())));

	}

}
