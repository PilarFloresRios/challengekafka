package com.soprasteria.challenge.producer.services;

import java.util.List;
import java.util.Map;

import com.soprasteria.challenge.producer.dtos.RequestShippingDTO;

public interface SendService {

	public abstract void sendModifiedOrders(Map<String, List<RequestShippingDTO>> map);
	
	public abstract void sendErrorOrders(String exception);
}
