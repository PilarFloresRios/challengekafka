package com.soprasteria.challenge.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

import com.soprasteria.challenge.consumer.entities.Order;
import com.soprasteria.challenge.consumer.streams.MessageStream;

@SpringBootApplication
@EnableBinding(value = { MessageStream.class, Order.class })
public class APIConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(APIConsumerApplication.class, args);
	}

}
