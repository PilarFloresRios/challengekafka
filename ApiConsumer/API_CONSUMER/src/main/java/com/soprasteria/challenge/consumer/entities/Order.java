package com.soprasteria.challenge.consumer.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.soprasteria.challenge.consumer.builders.OrderBuilder;

@Entity
@Table(name = "ORDERS")
public class Order {

	@Id
	@GeneratedValue
	@Column
	private long id;

	@Column(name = "COMPANY")
	private String company;

	
	@Column(name = "DATE")
	private LocalDate shippingDate;

	@Column(name = "CITY")
	private String shippingCity;

	@Column(name = "ADDRESS")
	private String shippingAddress;

	@Column(name = "WEIGHT")
	private float weight;

	public Order() {

	}



	public Order(long id, String company, LocalDate shippingDate, String shippingCity,
			String shippingAddress, float weight) {
		this.id = id;
		this.company = company;
		this.shippingDate = shippingDate;
		this.shippingCity = shippingCity;
		this.shippingAddress = shippingAddress;
		this.weight = weight;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getShippingCity() {
		return shippingCity;
	}

	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public LocalDate getShippingDate() {
		return shippingDate;
	}

	public void setShippingDate(LocalDate shippingDate) {
		this.shippingDate = shippingDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Order [id=");
		builder.append(id);
		builder.append(", company=");
		builder.append(company);
		builder.append(", shippingCity=");
		builder.append(shippingCity);
		builder.append(", shippingAddress=");
		builder.append(shippingAddress);
		builder.append(", weight=");
		builder.append(weight);
		builder.append("]");
		return builder.toString();
	}

	public Order(OrderBuilder builder) {
		
		this.company = builder.getCompany();
		this.shippingAddress = builder.getShippingAddress();
		this.shippingCity = builder.getShippingCity();
		this.shippingDate = builder.getShippingDate();
		this.weight = builder.getWeight();

	}

}
