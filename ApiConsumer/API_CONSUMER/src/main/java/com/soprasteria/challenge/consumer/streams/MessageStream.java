package com.soprasteria.challenge.consumer.streams;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface MessageStream {

	String INPUT = "message";

	String OUTPUT_OK_PROCESED = "procesed_orders";

	String OUTPUT_OK_CITY = "orders_by_city";

	String OUTPUT_KO_DB = "error_not_shared";

	@Input(INPUT)
	SubscribableChannel messageIn();

	@Output(OUTPUT_OK_PROCESED)
	MessageChannel messageProcesed();

	@Output(OUTPUT_OK_CITY)
	MessageChannel messageOrdersByCity();

	@Output(OUTPUT_KO_DB)
	MessageChannel messageErrorToShare();
}
