package com.soprasteria.challenge.consumer.listeners.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

import com.soprasteria.challenge.consumer.dtos.OrderDTO;
import com.soprasteria.challenge.consumer.listeners.OrderListener;
import com.soprasteria.challenge.consumer.services.HandleOrdersService;
import com.soprasteria.challenge.consumer.streams.MessageStream;

@Component
public class OrderListenerImpl implements OrderListener {

	private HandleOrdersService handleOrdersService;

	@Autowired
	public OrderListenerImpl(HandleOrdersService handleOrdersService) {
		this.handleOrdersService = handleOrdersService;
	}

	@StreamListener(MessageStream.INPUT)
	public void listenOrders(Map<String, List<OrderDTO>> ordersIn) {

		handleOrdersService.managerOrders(ordersIn);

	}

}
