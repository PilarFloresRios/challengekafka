package com.soprasteria.challenge.consumer.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soprasteria.challenge.consumer.dtos.OrderDTO;
import com.soprasteria.challenge.consumer.services.CheckOrdersService;
import com.soprasteria.challenge.consumer.services.HandleOrdersService;
import com.soprasteria.challenge.consumer.services.ProcessOrderService;

@Service
public class HandleOrdersServiceImpl implements HandleOrdersService {

	ProcessOrderService processOrderService;
	
	CheckOrdersService checkOrdersService;

	@Autowired
	public HandleOrdersServiceImpl(ProcessOrderService processOrderService, 
			CheckOrdersService checkOrdersService) {
		this.processOrderService = processOrderService;
		this.checkOrdersService = checkOrdersService;
	}

	@Override
	public void managerOrders(Map<String, List<OrderDTO>> orders) {

		processOrderService.recoverOrdersFromDB(orders);

		checkOrdersService.checkOrdersToShare(orders);

		checkOrdersService.checkToSendOrders(orders);

	}

}
