package com.soprasteria.challenge.consumer.builders;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

@Component
public class OrderBuilder {

	private long id;

	private String company;

	private LocalDate shippingDate;

	private String shippingCity;

	private String shippingAddress;

	private float weight;

	public OrderBuilder() {

	}

	public OrderBuilder withId(long id) {
		this.id = id;
		return this;
	}

	public OrderBuilder withCompany(String company) {
		this.company = company;
		return this;
	}

	public OrderBuilder withShippingDate(LocalDate date) {

		this.shippingDate = date;
		return this;
	}

	public OrderBuilder withShippingCity(String city) {
		this.shippingCity = city;
		return this;
	}

	public OrderBuilder withShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
		return this;
	}

	public OrderBuilder withWeight(float weight) {
		this.weight = weight;
		return this;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public LocalDate getShippingDate() {
		return shippingDate;
	}

	public void setShippingDate(LocalDate shippingDate) {
		this.shippingDate = shippingDate;
	}

	public String getShippingCity() {
		return shippingCity;
	}

	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

}
