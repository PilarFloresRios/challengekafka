package com.soprasteria.challenge.consumer.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soprasteria.challenge.consumer.builders.OrderBuilder;
import com.soprasteria.challenge.consumer.dtos.OrderDTO;
import com.soprasteria.challenge.consumer.entities.Order;
import com.soprasteria.challenge.consumer.repositories.OrderRepository;
import com.soprasteria.challenge.consumer.services.ProcessOrderService;

@Service
public class ProcessOrderServiceImpl implements ProcessOrderService {

	OrderRepository orderRepository;

	OrderBuilder orderBuilder;

	@Autowired
	public ProcessOrderServiceImpl(OrderRepository orderRepository, OrderBuilder orderBuilder) {
		this.orderRepository = orderRepository;
		this.orderBuilder = orderBuilder;
	}

	@Override
	public Map<String, List<OrderDTO>> recoverOrdersFromDB(Map<String, List<OrderDTO>> ordersIn) {

		
		ordersIn.forEach((city, ordersListPerCity) -> {

			orderRepository.findByShippingCity(city)
				.ifPresent(orderStored -> 
					orderStored.forEach(o -> {				
						ordersListPerCity.add(
								new OrderDTO(orderBuilder
										.withId(o.getId())
										.withCompany(o.getCompany())
										.withShippingAddress(o.getShippingAddress())
										.withShippingCity(o.getShippingCity())
										.withShippingDate(o.getShippingDate())
										.withWeight(o.getWeight())));
					}));

		});

		return ordersIn;
	}

	@Override
	public void shareInDB(List<OrderDTO> ordersToShare) {

		ordersToShare.stream()
			.filter(i -> i.getId() == 0)
			.forEach(i -> orderRepository.save(
							new Order(orderBuilder
										.withCompany(i.getCompany())
										.withShippingAddress(i.getShippingAddress())
										.withShippingCity(i.getShippingCity())
										.withShippingDate(i.getShippingDate())
										.withWeight(i.getWeight()))));

	}

	@Override
	public void deleteProcesedOrdesInDB(List<OrderDTO> ordersToDelete) {
		ordersToDelete.stream()
			.filter(i -> i.getId() != 0)
			.forEach(i -> orderRepository.deleteById(i.getId()));

	}

}
