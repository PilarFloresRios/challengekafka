package com.soprasteria.challenge.consumer.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.soprasteria.challenge.consumer.entities.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {

	Optional<List<Order>> findByShippingCity(String city);

}
