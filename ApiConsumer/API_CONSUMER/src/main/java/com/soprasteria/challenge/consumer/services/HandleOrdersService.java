package com.soprasteria.challenge.consumer.services;

import java.util.List;
import java.util.Map;

import com.soprasteria.challenge.consumer.dtos.OrderDTO;

public interface HandleOrdersService {

	public abstract void managerOrders(Map<String, List<OrderDTO>> ordersIn);

}
