package com.soprasteria.challenge.consumer.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

import com.soprasteria.challenge.consumer.dtos.OrderDTO;
import com.soprasteria.challenge.consumer.services.ShippingToTopicService;
import com.soprasteria.challenge.consumer.streams.MessageStream;


@Service
public class ShippingToTopicImpl implements ShippingToTopicService {

	MessageStream messageStream;

	@Autowired
	public ShippingToTopicImpl(MessageStream messageStream) {
		this.messageStream = messageStream;
	}

	@Override
	public void sendProcesedOrders(Map<String, List<OrderDTO>> processedOrders) {

		final MessageChannel messageChannel = messageStream.messageProcesed();

		messageChannel.send(MessageBuilder
				.withPayload(processedOrders)
				.setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
				.build());

	}

	@Override
	public void sendStoredOrders(List<OrderDTO> storedOrders) {

		final MessageChannel messageChannel = messageStream.messageOrdersByCity();

		messageChannel.send(MessageBuilder
				.withPayload("[" + storedOrders.get(0).getShippingCity() + ", number of stored orders:"
						+ storedOrders.size() + "]")
				.setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
				.build());

	}

	@Override
	public void sendDataBaseErrors(String e) {
		
		final MessageChannel messageChannel = messageStream.messageErrorToShare();
		messageChannel.send(MessageBuilder
				.withPayload(e.toString())
				.setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
				.build());
		
		
	}

}
