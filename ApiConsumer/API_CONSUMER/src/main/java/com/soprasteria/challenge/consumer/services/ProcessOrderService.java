package com.soprasteria.challenge.consumer.services;

import java.util.List;
import java.util.Map;

import com.soprasteria.challenge.consumer.dtos.OrderDTO;

public interface ProcessOrderService {

	public abstract void shareInDB(List<OrderDTO> ordersToShare);

	public abstract Map<String, List<OrderDTO>> recoverOrdersFromDB(Map<String, List<OrderDTO>> ordersIn);

	public abstract void deleteProcesedOrdesInDB(List<OrderDTO> ordersToDelete);

}
