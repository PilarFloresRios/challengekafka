package com.soprasteria.challenge.consumer.services.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionSystemException;

import com.soprasteria.challenge.consumer.dtos.OrderDTO;
import com.soprasteria.challenge.consumer.services.CheckOrdersService;
import com.soprasteria.challenge.consumer.services.ProcessOrderService;
import com.soprasteria.challenge.consumer.services.ShippingToTopicService;

@Service
public class CheckOrdersServiceImpl implements CheckOrdersService{

	ProcessOrderService processOrdersServce;
	
	ShippingToTopicService shippingToTopicService;
	
	@Autowired
	public CheckOrdersServiceImpl(ProcessOrderService processOrdersServce, 
			ShippingToTopicService shippingToTopicService) {
		this.processOrdersServce = processOrdersServce;
		this.shippingToTopicService = shippingToTopicService;
	}

	@Override
	public void checkOrdersToShare(Map<String, List<OrderDTO>> orders) {
		orders.values().stream()
		.filter(l -> 
			(l.size() < 10) 
			&& (l.stream().map(OrderDTO::getWeight).reduce(0f, Float::sum) < 15f))
		.forEach(l -> {
				try {
					processOrdersServce.shareInDB(l);
					shippingToTopicService.sendStoredOrders(l);				
				}catch (TransactionSystemException e) {
					shippingToTopicService.sendDataBaseErrors(e.getCause().getCause().toString());
				}catch (Exception e) {
					shippingToTopicService.sendDataBaseErrors(e.toString());
				}
				
		});
		
	}

	@Override
	public void checkToSendOrders(Map<String, List<OrderDTO>> orders) {
		orders.values().stream()
		.filter(l -> (l.size() > 9) 
				|| (l.stream().map(OrderDTO::getWeight).reduce(0f, Float::sum) >= 15f))
		.forEach(l -> {
			processOrdersServce.deleteProcesedOrdesInDB(l);
			shippingToTopicService.sendProcesedOrders(
					l.stream().collect(Collectors.groupingBy(OrderDTO::getShippingCity)));
		});
	}

		
	

}
