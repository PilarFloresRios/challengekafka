package com.soprasteria.challenge.consumer.dtos;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.soprasteria.challenge.consumer.builders.OrderBuilder;

public class OrderDTO {

	private long id;

	private String company;

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonFormat(pattern = "dd/MM/yy")
	private LocalDate shippingDate;

	private String shippingCity;
	

	@NotNull
	private String shippingAddress;

	private float weight;

	public OrderDTO() {

	}

	public OrderDTO(String company, LocalDate shippingDate, 
			String shippingCity, String shippingAddress, 
			float weight, long id) {
		this.company = company;
		this.shippingDate = shippingDate;
		this.shippingCity = shippingCity;
		this.shippingAddress = shippingAddress;
		this.weight = weight;
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public LocalDate getShippingDate() {
		return shippingDate;
	}

	public void setShippingDate(LocalDate shippingDate) {
		this.shippingDate = shippingDate;
	}

	public String getShippingCity() {
		return shippingCity;
	}

	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OrderDTO [id=");
		builder.append(id);
		builder.append(", company=");
		builder.append(company);
		builder.append(", shippingDate=");
		builder.append(shippingDate);
		builder.append(", shippingCity=");
		builder.append(shippingCity);
		builder.append(", shippingAddress=");
		builder.append(shippingAddress);
		builder.append(", weight=");
		builder.append(weight);
		builder.append("]");
		return builder.toString();
	}

	public OrderDTO(OrderBuilder builder) {
		this.id = builder.getId();
		this.company = builder.getCompany();
		this.shippingAddress = builder.getShippingAddress();
		this.shippingCity = builder.getShippingCity();
		this.shippingDate = builder.getShippingDate();
		this.weight = builder.getWeight();
	}

}
