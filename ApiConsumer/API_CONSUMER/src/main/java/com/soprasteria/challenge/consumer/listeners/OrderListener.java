package com.soprasteria.challenge.consumer.listeners;

import java.util.List;
import java.util.Map;

import com.soprasteria.challenge.consumer.dtos.OrderDTO;

public interface OrderListener {

	public void listenOrders(Map<String, List<OrderDTO>> ordersIn);

}
