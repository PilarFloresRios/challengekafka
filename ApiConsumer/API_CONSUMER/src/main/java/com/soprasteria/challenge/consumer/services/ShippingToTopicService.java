package com.soprasteria.challenge.consumer.services;

import java.util.List;
import java.util.Map;

import com.soprasteria.challenge.consumer.dtos.OrderDTO;

public interface ShippingToTopicService {

	public abstract void sendProcesedOrders(Map<String, List<OrderDTO>> processedOrders);

	public abstract void sendStoredOrders(List<OrderDTO> storedOrders);

	public abstract void sendDataBaseErrors(String e);
}
