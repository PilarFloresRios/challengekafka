package com.soprasteria.challenge.consumer.services;

import java.util.List;
import java.util.Map;

import com.soprasteria.challenge.consumer.dtos.OrderDTO;

public interface CheckOrdersService {

	public abstract void checkOrdersToShare(Map<String, List<OrderDTO>> orders);

	public abstract void checkToSendOrders(Map<String, List<OrderDTO>> orders);
}
