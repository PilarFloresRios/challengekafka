package com.soprasteria.challenge.consumer.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.soprasteria.challenge.consumer.entities.Order;
import com.soprasteria.challenge.consumer.utils.Generators;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OrderRepositoryTest {
	
	@Autowired
	private OrderRepository orderRepository;
	
	
	@Test
	public void saveOrderTest() {
		
		Order order = orderRepository.save(Generators.createOrderComplete());
	
		assertThat(order).hasFieldOrProperty("id");
		assertThat(order).hasFieldOrProperty("company");
		assertThat(order).hasFieldOrProperty("shippingAddress");
		assertThat(order).hasFieldOrProperty("shippingCity");
		assertThat(order).hasFieldOrProperty("weight");
		
		assertThat(order).hasFieldOrPropertyWithValue("company", "Compañia de prueba");
		assertThat(order).hasFieldOrPropertyWithValue("shippingAddress", "Direccion de prueba");
		assertThat(order).hasFieldOrPropertyWithValue("shippingCity", "Ciudad de prueba");
		assertThat(order).hasFieldOrPropertyWithValue("weight", 1f);
		assertThat(order).hasFieldOrPropertyWithValue("shippingDate", LocalDate.now());
	
	}
	
	@Test
	public void findOrderByShippingCityAndRetunrListWithOrder(){
		Order order = orderRepository.save(Generators.createOrderComplete());
		
		Optional<List<Order>> orderFound = orderRepository.findByShippingCity("Ciudad de prueba");
	
		  assertEquals(1, orderFound.get().size());
		  assertEquals(order.getCompany(), orderFound.get().get(0).getCompany());
		  assertEquals(order.getShippingAddress(), orderFound.get().get(0).getShippingAddress());
		  assertEquals(order.getShippingCity(), orderFound.get().get(0).getShippingCity());
		  assertEquals(order.getShippingDate(), orderFound.get().get(0).getShippingDate());

		
	}

}
