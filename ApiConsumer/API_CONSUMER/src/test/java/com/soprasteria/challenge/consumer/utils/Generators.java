package com.soprasteria.challenge.consumer.utils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.soprasteria.challenge.consumer.builders.OrderBuilder;
import com.soprasteria.challenge.consumer.dtos.OrderDTO;
import com.soprasteria.challenge.consumer.entities.Order;

public class Generators {

	public static List<OrderDTO> createRequestInComplete() {
		List<OrderDTO> requestIn = new ArrayList<>();

		requestIn.add(createOrderDTOComplete());

		return requestIn;
	}
	
	public static List<OrderDTO> createRequestInCompleteWithId() {
		List<OrderDTO> requestIn = new ArrayList<>();

		requestIn.add(createOrderDTOCompleteWithId());

		return requestIn;
	}

	public static List<OrderDTO> createRequestInCompleteWith10Orders() {
		List<OrderDTO> requestIn = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			requestIn.add(createOrderDTOComplete());
		}

		return requestIn;
	}

	public static List<OrderDTO> createRequestInCompleteOverWeight() {
		List<OrderDTO> requestIn = new ArrayList<>();

		requestIn.add(createOrderDTOCompleteOverWeight());

		return requestIn;
	}

	public static List<Order> createListOfOrders() {
		List<Order> list = new ArrayList<>();

		list.add(new Order(new OrderBuilder()
				.withCompany("Company")
				.withId(1L)
				.withShippingAddress("shippingAddress")
				.withShippingCity("Ciudad de prueba")
				.withShippingDate(LocalDate.now())
				.withWeight(3)));

		return list;
	}
	
	public static OrderDTO createOrderDTOComplete() {

		OrderDTO dto = new OrderDTO(
				new OrderBuilder().withCompany("Compañia de prueba").withShippingAddress("Direccion de prueba")
						.withShippingCity("Ciudad de prueba").withShippingDate(LocalDate.now()).withWeight(1f));

		return dto;
	}
	
	public static OrderDTO createOrderDTOCompleteWithId() {

		OrderDTO dto = new OrderDTO(
				new OrderBuilder().withId(1L).withCompany("Compañia de prueba").withShippingAddress("Direccion de prueba")
						.withShippingCity("Ciudad de prueba").withShippingDate(LocalDate.now()).withWeight(1f));

		return dto;
	}

	public static OrderDTO createOrderDTOCompleteOverWeight() {

		OrderDTO dto = new OrderDTO(
				new OrderBuilder().withCompany("Compañia de prueba").withShippingAddress("Direccion de prueba")
						.withShippingCity("Ciudad de prueba").withShippingDate(LocalDate.now()).withWeight(100f));

		return dto;
	}

	public static Map<String, List<OrderDTO>> createMapOrders() {
		Map<String, List<OrderDTO>> map = new HashMap<String, List<OrderDTO>>();

		map.put("Ciudad de prueba", createRequestInComplete());
		return map;
	}

	public static Map<String, List<OrderDTO>> createMapWith10Orders() {
		Map<String, List<OrderDTO>> map = new HashMap<String, List<OrderDTO>>();

		map.put("Ciudad de prueba", createRequestInCompleteWith10Orders());
		return map;
	}

	public static Map<String, List<OrderDTO>> createMapOrdersOverWeight() {
		Map<String, List<OrderDTO>> map = new HashMap<String, List<OrderDTO>>();

		map.put("Ciudad de prueba", createRequestInCompleteOverWeight());
		return map;
	}
	
	public static Order createOrderComplete() {
		
		return new Order(
				new OrderBuilder()
				.withCompany("Compañia de prueba")
				.withShippingAddress("Direccion de prueba")
				.withShippingCity("Ciudad de prueba")
				.withShippingDate(LocalDate.now())
				.withWeight(1f));

		
	}
}
