package com.soprasteria.challenge.consumer.services;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.soprasteria.challenge.consumer.dtos.OrderDTO;
import com.soprasteria.challenge.consumer.services.impl.CheckOrdersServiceImpl;
import com.soprasteria.challenge.consumer.utils.Generators;

@RunWith(MockitoJUnitRunner.class)
public class CheckOrdersServiceTest {

	@Mock
	ProcessOrderService processOrdersService;

	@Mock
	ShippingToTopicService shippingToTopicService;

	@InjectMocks
	CheckOrdersService checkOrdersService = new CheckOrdersServiceImpl(processOrdersService, shippingToTopicService);

	@Test
	public void whenCheckOrdersToShareWithout10OrdersNeitherEnoughtWeight_thenShareOrders() {

		Map<String, List<OrderDTO>> mapIn = Generators.createMapOrders();

		checkOrdersService.checkOrdersToShare(mapIn);

		verify(processOrdersService, times(1)).shareInDB(mapIn.get("Ciudad de prueba"));
		verify(shippingToTopicService, times(1)).sendStoredOrders(mapIn.get("Ciudad de prueba"));
	}
	
	
	@Test
	public void whenCheckOrdersToShareWith10Orders_thenNotShareOrders() {
		
		Map<String, List<OrderDTO>> mapIn = Generators.createMapWith10Orders();
		checkOrdersService.checkOrdersToShare(mapIn);
		
		verify(processOrdersService, times(0)).shareInDB(mapIn.get("Ciudad de prueba"));
		verify(shippingToTopicService, times(0)).sendStoredOrders(mapIn.get("Ciudad de prueba"));
		
	}
	
	@Test
	public void whenCheckOrdersToShareWithOverWeight_thenNotShareOrders() {
		
		Map<String, List<OrderDTO>> mapIn = Generators.createMapOrdersOverWeight();
		checkOrdersService.checkOrdersToShare(mapIn);
		
		verify(processOrdersService, times(0)).shareInDB(mapIn.get("Ciudad de prueba"));
		verify(shippingToTopicService, times(0)).sendStoredOrders(mapIn.get("Ciudad de prueba"));
		
	}
	
	
	@Test
	public void whenCheckOrdersToSendWithout10OrdersNeitherEnoughtWeight_thenNotSendOrders() {

		Map<String, List<OrderDTO>> mapIn = Generators.createMapOrders();

		checkOrdersService.checkToSendOrders(mapIn);

		verify(processOrdersService, times(0)).deleteProcesedOrdesInDB(mapIn.get("Ciudad de prueba"));
	}
	
	
	@Test
	public void whenCheckOrdersToSendWith10Orders_thenSendOrders() {

		Map<String, List<OrderDTO>> mapIn = Generators.createMapWith10Orders();

		checkOrdersService.checkToSendOrders(mapIn);

		verify(processOrdersService, times(1)).deleteProcesedOrdesInDB(mapIn.get("Ciudad de prueba"));
	}
	
	@Test
	public void whenCheckOrdersToSendWithOverWeight_thenSendOrders() {

		Map<String, List<OrderDTO>> mapIn = Generators.createMapOrdersOverWeight();

		checkOrdersService.checkToSendOrders(mapIn);

		verify(processOrdersService, times(1)).deleteProcesedOrdesInDB(mapIn.get("Ciudad de prueba"));
	}
	
	

}
