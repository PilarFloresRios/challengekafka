package com.soprasteria.challenge.consumer.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.soprasteria.challenge.consumer.builders.OrderBuilder;
import com.soprasteria.challenge.consumer.utils.Generators;

@RunWith(MockitoJUnitRunner.class)
public class OrderTest {
	
	
	private static final String COMPANY = "COMPANY";
	private static final LocalDate SHIPPINGDATE = LocalDate.now();	
	private static final String SHIPPINGCITY = "SHIPPING_CITY";
	private static final String SHIPPINGADDRESS = "SHIPPING_ADDRESS";
	private static final float WEIGHT = 1f;
	
	@Test
	public void orderGettersAnDSettersTest() {
		
		Order order = new Order();
		order.setCompany(COMPANY);
		order.setShippingAddress(SHIPPINGADDRESS);
		order.setShippingCity(SHIPPINGCITY);
		order.setShippingDate(SHIPPINGDATE);
		order.setWeight(WEIGHT);
		order.setId(1L);
		
		
		/* Setters*/
		assertNotNull("Testing Order setter Company", order.getCompany());
		assertNotNull("Testing Order setter ShippingAddress", order.getShippingAddress());
		assertNotNull("Testing Order setter ShippingCity", order.getShippingCity());
		assertNotNull("Testing Order setter ShippingDate", order.getShippingDate());
		assertNotNull("Testing Order setter Weight", order.getWeight());
		assertNotNull("Testing Order setter Id", order.getId());
		
		
		/* Getters */
		assertEquals("Testing Order getter Company", COMPANY,  order.getCompany());
		assertEquals("Testing Order getter ShippingAddress", SHIPPINGADDRESS,  order.getShippingAddress());
		assertEquals("Testing Order getter ShippingCity", SHIPPINGCITY,  order.getShippingCity());
		assertEquals("Testing Order getter ShippingDate", SHIPPINGDATE,  order.getShippingDate());
		assertEquals("Testing Order getter Id", 1L,  order.getId());
	}
	
	@Test
	public void orderToStringTest() {
		
		Order order = Generators.createOrderComplete();
		
		String expected = "Order [id=0, company=Compañia de prueba, shippingCity=Ciudad de prueba, shippingAddress=Direccion de prueba, weight=1.0]";
		
		assertNotNull("Testing method order().toString()", order.toString());
		assertEquals("Testing method order().toString()", expected, order.toString());
	}
	
	@Test
	public void orderBuilderTest() {
		
		OrderBuilder buider = new OrderBuilder();
		
		Order order = new Order(buider
				.withCompany(COMPANY)
				.withShippingAddress(SHIPPINGADDRESS)
				.withShippingCity(SHIPPINGCITY)
				.withShippingDate(SHIPPINGDATE)
				.withWeight(WEIGHT));
		
		assertEquals("Testing Order Builder Company", COMPANY,  order.getCompany());
		assertEquals("Testing Order Builder ShippingAddress", SHIPPINGADDRESS,  order.getShippingAddress());
		assertEquals("Testing Order Builder ShippingCity", SHIPPINGCITY,  order.getShippingCity());
		assertEquals("Testing Order Builder ShippingDate", SHIPPINGDATE,  order.getShippingDate());
		
	}
	
	
	@Test
	public void orderConstructorTest() {
		Order order = new Order(1L, COMPANY, SHIPPINGDATE, SHIPPINGCITY, SHIPPINGADDRESS, WEIGHT);
	
		assertEquals("Testing Order Builder Company", COMPANY,  order.getCompany());
		assertEquals("Testing Order Builder ShippingAddress", SHIPPINGADDRESS,  order.getShippingAddress());
		assertEquals("Testing Order Builder ShippingCity", SHIPPINGCITY,  order.getShippingCity());
		assertEquals("Testing Order Builder ShippingDate", SHIPPINGDATE,  order.getShippingDate());
		assertEquals("Testing Order getter Id", 1L,  order.getId());
	
	}

}
