package com.soprasteria.challenge.consumer.dtos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;

import org.junit.Test;

import com.soprasteria.challenge.consumer.builders.OrderBuilder;
import com.soprasteria.challenge.consumer.utils.Generators;

public class OrderDTOTest {

	private static final String COMPANY = "COMPANY";
	private static final LocalDate SHIPPINGDATE = LocalDate.now();	
	private static final String SHIPPINGCITY = "SHIPPING_CITY";
	private static final String SHIPPINGADDRESS = "SHIPPING_ADDRESS";
	private static final float WEIGHT = 1f;
	
	
	@Test
	public void orderDTOGettersAnDSettersTest() {
		
		OrderDTO order = new OrderDTO();
		order.setCompany(COMPANY);
		order.setShippingAddress(SHIPPINGADDRESS);
		order.setShippingCity(SHIPPINGCITY);
		order.setShippingDate(SHIPPINGDATE);
		order.setWeight(WEIGHT);
		order.setId(1L);
		
		
		/* Setters*/
		assertNotNull("Testing Order setter Company", order.getCompany());
		assertNotNull("Testing Order setter ShippingAddress", order.getShippingAddress());
		assertNotNull("Testing Order setter ShippingCity", order.getShippingCity());
		assertNotNull("Testing Order setter ShippingDate", order.getShippingDate());
		assertNotNull("Testing Order setter Weight", order.getWeight());
		assertNotNull("Testing Order setter Id", order.getId());
		
		
		/* Getters */
		assertEquals("Testing Order getter Company", COMPANY,  order.getCompany());
		assertEquals("Testing Order getter ShippingAddress", SHIPPINGADDRESS,  order.getShippingAddress());
		assertEquals("Testing Order getter ShippingCity", SHIPPINGCITY,  order.getShippingCity());
		assertEquals("Testing Order getter ShippingDate", SHIPPINGDATE,  order.getShippingDate());
		assertEquals("Testing Order getter Id", 1L,  order.getId());
	}
	
	
	@Test
	public void orderDTOToStringTest() {
		
		OrderDTO order = Generators.createOrderDTOComplete();
		
		String expected = "OrderDTO [id=0, company=Compañia de prueba, shippingDate=2019-10-29, shippingCity=Ciudad de prueba, shippingAddress=Direccion de prueba, weight=1.0]";
		
		assertNotNull("Testing method order().toString()", order.toString());
		assertEquals("Testing method order().toString()", expected, order.toString());
	}
	
	@Test
	public void orderDTOBuilderTest() {
		
		OrderBuilder buider = new OrderBuilder();
		
		OrderDTO order = new OrderDTO(buider
				.withCompany(COMPANY)
				.withShippingAddress(SHIPPINGADDRESS)
				.withShippingCity(SHIPPINGCITY)
				.withShippingDate(SHIPPINGDATE)
				.withWeight(WEIGHT));
		
		assertEquals("Testing Order Builder Company", COMPANY,  order.getCompany());
		assertEquals("Testing Order Builder ShippingAddress", SHIPPINGADDRESS,  order.getShippingAddress());
		assertEquals("Testing Order Builder ShippingCity", SHIPPINGCITY,  order.getShippingCity());
		assertEquals("Testing Order Builder ShippingDate", SHIPPINGDATE,  order.getShippingDate());
		
	}
	
	@Test
	public void orderDTOConstructorTest() {
		OrderDTO order = new OrderDTO(COMPANY, SHIPPINGDATE, SHIPPINGCITY, SHIPPINGADDRESS, WEIGHT, 1L);
	
		assertEquals("Testing Order Builder Company", COMPANY,  order.getCompany());
		assertEquals("Testing Order Builder ShippingAddress", SHIPPINGADDRESS,  order.getShippingAddress());
		assertEquals("Testing Order Builder ShippingCity", SHIPPINGCITY,  order.getShippingCity());
		assertEquals("Testing Order Builder ShippingDate", SHIPPINGDATE,  order.getShippingDate());
		assertEquals("Testing Order getter Id", 1L,  order.getId());
	
	}
	
}
