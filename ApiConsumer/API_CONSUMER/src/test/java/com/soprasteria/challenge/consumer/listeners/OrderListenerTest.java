package com.soprasteria.challenge.consumer.listeners;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import com.soprasteria.challenge.consumer.dtos.OrderDTO;
import com.soprasteria.challenge.consumer.services.HandleOrdersService;
import com.soprasteria.challenge.consumer.services.ShippingToTopicService;
import com.soprasteria.challenge.consumer.streams.MessageStream;
import com.soprasteria.challenge.consumer.utils.Generators;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class OrderListenerTest {

	@SpyBean
	private OrderListener orderListener;

	@Autowired
	private MessageStream messageStream;

	@Mock
	HandleOrdersService handleOrdersService;

	@Mock
	ShippingToTopicService shippingToTopicService;

	@Test
	public void listenOrdersTest() {

		Map<String, List<OrderDTO>> message = Generators.createMapOrders();
		messageStream.messageIn().send(MessageBuilder.withPayload(message).build());

		doNothing().when(shippingToTopicService).sendStoredOrders(Mockito.any());

		verify(this.orderListener, times(1)).listenOrders(Mockito.any());
	}

}
