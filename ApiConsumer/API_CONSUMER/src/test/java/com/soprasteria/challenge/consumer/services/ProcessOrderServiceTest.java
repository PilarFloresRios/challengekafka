package com.soprasteria.challenge.consumer.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.soprasteria.challenge.consumer.builders.OrderBuilder;
import com.soprasteria.challenge.consumer.dtos.OrderDTO;
import com.soprasteria.challenge.consumer.entities.Order;
import com.soprasteria.challenge.consumer.repositories.OrderRepository;
import com.soprasteria.challenge.consumer.services.impl.ProcessOrderServiceImpl;
import com.soprasteria.challenge.consumer.utils.Generators;

@RunWith(MockitoJUnitRunner.class)
public class ProcessOrderServiceTest {

	@Mock
	OrderRepository orderRepository;

	OrderBuilder orderBuilder = new OrderBuilder();

	@InjectMocks
	ProcessOrderServiceImpl service = new ProcessOrderServiceImpl(orderRepository, orderBuilder);

	@Test
	public void givenCityWithSharedOrders_thenRecoverOrderFromDB_thenReturnAllOrders() {

		Map<String, List<OrderDTO>> mapIn = Generators.createMapOrders();

		List<Order> list = Generators.createListOfOrders();

		when(orderRepository.findByShippingCity("Ciudad de prueba")).thenReturn(Optional.of(list));

		assertEquals(2, service.recoverOrdersFromDB(mapIn).get("Ciudad de prueba").size());
	}

	@Test
	public void givenCityWithoutSharedOrders_whenShareOrdersInDB_thenReturnAllOrders() {

		Map<String, List<OrderDTO>> mapIn = Generators.createMapOrders();

		when(orderRepository.findByShippingCity("Ciudad de prueba")).thenReturn(Optional.ofNullable(null));

		assertEquals(1, service.recoverOrdersFromDB(mapIn).get("Ciudad de prueba").size());
	}

	@Test
	public void givenListOrderToShare_whenShareOrdersInDB_thenShareOrder() {

		List<OrderDTO> list = Generators.createRequestInComplete();
		service.shareInDB(list);

		verify(orderRepository, times(1)).save(Mockito.any());
	}

	@Test
	public void givenListSharedOrder_whenShareOrdersInDB_thenNotShare() {

		List<OrderDTO> list = Generators.createRequestInCompleteWithId();
		service.shareInDB(list);

		verify(orderRepository, times(0)).save(Mockito.any());
	}

	@Test
	public void givenListOrderToDelete_whenDeleteOrdersInDB_thenDeleteOrder() {

		List<OrderDTO> list = Generators.createRequestInCompleteWithId();
		service.deleteProcesedOrdesInDB(list);

		verify(orderRepository, times(1)).deleteById(Mockito.anyLong());
	}
	
	@Test
	public void givenListOrderNotShare_whenDeleteOrdersInDB_thenNotDeleteOrder() {

		List<OrderDTO> list = Generators.createRequestInComplete();
		service.deleteProcesedOrdesInDB(list);

		verify(orderRepository, times(0)).deleteById(Mockito.anyLong());
	}

}
