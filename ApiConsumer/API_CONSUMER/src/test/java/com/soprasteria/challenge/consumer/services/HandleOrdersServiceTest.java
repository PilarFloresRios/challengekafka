package com.soprasteria.challenge.consumer.services;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.soprasteria.challenge.consumer.dtos.OrderDTO;
import com.soprasteria.challenge.consumer.services.impl.HandleOrdersServiceImpl;
import com.soprasteria.challenge.consumer.utils.Generators;

@RunWith(MockitoJUnitRunner.class)
public class HandleOrdersServiceTest {

	@Mock
	ProcessOrderService processOrderService;

	@Mock
	CheckOrdersService checkOrdersService;

	@InjectMocks
	HandleOrdersService service = new HandleOrdersServiceImpl(processOrderService, checkOrdersService);

	@Test
	public void givenMessageCorrect_whenHandleOrders_thenCallsAllMethods() {

		Map<String, List<OrderDTO>> map = Generators.createMapOrders();

		service.managerOrders(map);

		verify(processOrderService, times(1)).recoverOrdersFromDB(map);

		verify(checkOrdersService, times(1)).checkOrdersToShare(map);

		verify(checkOrdersService, times(1)).checkToSendOrders(map);

	}

}
