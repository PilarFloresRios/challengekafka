package com.soprasteria.challenge.consumer.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.cloud.stream.test.binder.MessageCollector;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.soprasteria.challenge.consumer.dtos.OrderDTO;
import com.soprasteria.challenge.consumer.streams.MessageStream;
import com.soprasteria.challenge.consumer.utils.Generators;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ShippingToTopicTest {

	@Autowired
	private MessageCollector messageCollector;

	@Autowired
	private MessageStream messageStream;

	@SpyBean
	ShippingToTopicService service;


	@Test
	public void sendStoredOrders() throws JsonParseException, JsonMappingException, IOException {

		List<OrderDTO> message = Generators.createRequestInComplete();

		messageStream.messageOrdersByCity().send(MessageBuilder.withPayload(message).build());

		String receivedMsg = (String) messageCollector.forChannel(messageStream.messageOrdersByCity()).poll()
				.getPayload();

		service.sendStoredOrders(message);

		assertThat(this.service).isNotNull();
		assertThat(receivedMsg).isNotNull();
	}

	@Test
	public void sendDataBaseErrors() throws JsonParseException, JsonMappingException, IOException {

		String message = "Mensaje Error";

		messageStream.messageErrorToShare().send(MessageBuilder.withPayload(message).build());

		String receivedMsg = (String) messageCollector.forChannel(messageStream.messageErrorToShare()).poll()
				.getPayload();

		service.sendDataBaseErrors(message);

		assertThat(this.service).isNotNull();
		assertThat(receivedMsg).isNotNull();
	}
	
	@Test
	public void sendProcesedOrders() throws JsonParseException, JsonMappingException, IOException {


		Map<String, List<OrderDTO>> map = Generators.createMapOrders();
		messageStream.messageProcesed().send(MessageBuilder.withPayload(map).build());

		String receivedMsg = (String) messageCollector.forChannel(messageStream.messageProcesed()).poll().getPayload();

		service.sendProcesedOrders(map);
		assertThat(this.service).isNotNull();
		assertThat(receivedMsg).isNotNull();
	}


}
